import psycopg2
import requests
import time
from tqdm import tqdm
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

# Database connection parameters
db_params = {   
    "host": "172.30.227.205",
    "port": 5439,
    "database": "rwh_gis_database",
    "user": "rwh_analytics",
    "password": "4HPzQt2HyU@",
}

# Create a database connection
try:
    conn = psycopg2.connect(**db_params)
except psycopg2.Error as e:
    print("Error connecting to the database:", e)
    exit(1)

# Create a cursor object
cursor = conn.cursor()

# Define the table creation SQL statement
create_table_query = """
CREATE TABLE IF NOT EXISTS bins_data_taldau (
    bin TEXT PRIMARY KEY,
    name TEXT,
    register_date DATE,
    oked_code TEXT,
    oked_name TEXT,
    krp_code TEXT,
    krp_name TEXT,
    kse_code TEXT,
    kse_name TEXT,
    kfs_code TEXT,
    kfs_name TEXT,
    kato_code TEXT,
    kato_id INTEGER,
    kato_address TEXT,
    fio TEXT,
    ip BOOLEAN
)
"""

# Execute the table creation query
cursor.execute(create_table_query)
conn.commit()

# Query the database to fetch BINs
query = "SELECT bin_code FROM bins_company_almaty"
cursor.execute(query)
bins = cursor.fetchall()

# Define the API endpoint
base_url = "https://old.stat.gov.kz/api/juridical/counter/api/"

# Create a session with retry mechanism
session = requests.Session()
retries = Retry(total=5, backoff_factor=1, status_forcelist=[500, 502, 503, 504])
session.mount('https://', HTTPAdapter(max_retries=retries))

# Process each retrieved BIN with a progress bar
for bin_code in tqdm(bins):
    bin = bin_code[0]  # Extract the BIN from the tuple

    # Create the API request URL
    api_url = f"{base_url}?bin={bin}&lang=ru"

    try:
        # Make the GET request using the session
        response = session.get(api_url)

        if response.status_code == 200:
            data = response.json()

            if data is not None and 'obj' in data:
                try:
                    # Check if the BIN already exists in the database
                    check_query = "SELECT bin FROM bins_data_taldau WHERE bin = %s"
                    cursor.execute(check_query, (data['obj']['bin'],))
                    existing_bin = cursor.fetchone()

                    if existing_bin:
                        tqdm.write(f"Data for BIN {bin} already exists in the database. Skipping...")
                    else:
                        # Insert the data into the database
                        insert_query = """
                        INSERT INTO bins_data_taldau (bin, name, register_date, oked_code, oked_name, krp_code, krp_name, kse_code, kse_name, kfs_code, kfs_name, kato_code, kato_id, kato_address, fio, ip)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                        """
                        values = (
                            data['obj']['bin'],
                            data['obj']['name'],
                            data['obj']['registerDate'],
                            data['obj']['okedCode'],
                            data['obj']['okedName'],
                            data['obj']['krpCode'],
                            data['obj']['krpName'],
                            data['obj']['kseCode'],
                            data['obj']['kseName'],
                            data['obj']['kfsCode'],
                            data['obj']['kfsName'],
                            data['obj']['katoCode'],
                            data['obj']['katoId'],
                            data['obj']['katoAddress'],
                            data['obj']['fio'],
                            data['obj']['ip']
                        )

                        cursor.execute(insert_query, values)
                        conn.commit()
                        tqdm.write(f"Data inserted for BIN: {bin}")
                except TypeError:
                    tqdm.write(f"Skipping BIN {bin} due to TypeError")
            else:
                tqdm.write(f"Invalid data for BIN: {bin}")
        else:
            tqdm.write(f"Failed to retrieve data for BIN: {bin}")
    except requests.exceptions.RequestException as e:
        tqdm.write(f"Request for BIN {bin} failed: {str(e)}")
        continue

    # Add a 1-second delay before the next request
    time.sleep(1)

# Close the database connection
cursor.close()
conn.close()

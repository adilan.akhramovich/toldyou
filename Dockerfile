FROM python:3.10

WORKDIR /app

COPY requirements.txt .

RUN apt-get update && apt-get install -y libpq-dev

RUN pip install --no-cache-dir -r requirements.txt

COPY taldau_bin.py .

CMD [ "python", "-u", "taldau_bin.py" ]
